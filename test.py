import cv2

LOW_RED = (17, 50, 110)
HIGH_RED = (170, 140, 180)


def get_color_coordinates(img_array, min_range, max_range):
    only_red = cv2.inRange(img_array, min_range, max_range)

    moments = cv2.moments(only_red)

    x_moment = moments['m01']
    y_moment = moments['m10']
    area = moments['m00']
    x = int(x_moment / area)
    y = int(y_moment / area)

    return y, x


img = cv2.imread("test.png")

x, y = get_color_coordinates(img, LOW_RED, HIGH_RED)
print(x, y)

cv2.putText(img, ".", (x, y), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0))

cv2.imshow('test', img)
cv2.waitKey(0)

