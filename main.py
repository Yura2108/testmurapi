import pymurapi as mur_api
from Submarine import Submarine
from ISubmarine import ISubmarine


def main():
    submarine: ISubmarine = Submarine(mur_api.mur_init())
    submarine.infinity_loop()


if __name__ == '__main__':
    main()
