from abc import ABC, abstractmethod


class ISubmarine(ABC):

    @abstractmethod
    def keep_depth(self, depth_value: int, power_value: int = 50) -> None:
        """
        Держит/Достигает глубину около указанного значения.
        :param power_value: Сила тяги
        :param depth_value: значение глубины
        """
        pass

    @property
    @abstractmethod
    def dive_power(self) -> int:
        """
        :return: Мощность моторов, которая будет установлена при стабилизации глубины
        """

    @property
    @abstractmethod
    def min_range(self) -> int:
        """
        :return: Минимальная граница в пикселях, где может находиться таргет (Нам нужен центр по Y)
        """

    @property
    @abstractmethod
    def max_range(self) -> int:
        """
        :return: Максимальная граница в пикселях, где может находиться таргет (Нам нужен центр по Y)
        """

    @staticmethod
    @abstractmethod
    def get_color_coordinates(img_array, min_range: tuple, max_range: tuple):
        """
        Ищет координаты цвета на изображении.
        :param img_array: Изображение в массиве NumPy
        :param min_range: Кортеж из 3 элементов - RGB граница цвета (Минимальный оттенок)
        :param max_range: Кортеж из 3 элементов - RGB граница цвета (Максимальный оттенок)
        :return: X, Y координаты найденного объекта (центр), либо 0, 0 если ничего не нашёл
        """
        pass

    @abstractmethod
    def infinity_loop(self):
        """
        Запускает бесконечный цикл, выполняющий все основные действия
        """