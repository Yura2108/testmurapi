from pymurapi.api import MurApiBase
import ISubmarine
import cv2
import time


class Submarine(ISubmarine.ISubmarine):
    # LOW_RED = (17, 50, 110)
    # HIGH_RED = (101, 140, 180)

    LOW_RED = (17, 50, 110)
    HIGH_RED = (170, 140, 180)

    RANGE_ACCURACY = 20
    IMAGE_CENTER = 1080 / 2

    STATES = ["HIGHER", "LOWER", "CENTER", "UNKNOWN"]

    # 0-3 indexes
    motor_powers = [0, 0, 0, 0, 0]

    DEBUG_MODE = False

    def __init__(self, mur_api: MurApiBase):
        self._mur_api = mur_api

        self.is_dived_up = False
        self.dive_depth = 0.0

    @property
    def dive_power(self):
        return 25

    @property
    def min_range(self):
        return self.IMAGE_CENTER - self.RANGE_ACCURACY

    @property
    def max_range(self):
        return self.IMAGE_CENTER + self.RANGE_ACCURACY

    @staticmethod
    def get_color_coordinates(img_array, min_range, max_range):
        try:
            only_red = cv2.inRange(img_array, min_range, max_range)

            moments = cv2.moments(only_red)
            x_moment = moments['m01']
            y_moment = moments['m10']
            area = moments['m00']
            x = int(x_moment / area)
            y = int(y_moment / area)

            return y, x
        finally:
            return 0, 0

    @staticmethod
    def get_color_coordinates_new(img_array, min_range, max_range):
        img_hsv = cv2.cvtColor(img_array, cv2.COLOR_BGR2HSV)

        img_bin = cv2.inRange(img_hsv, min_range, max_range)
        cnt, _ = cv2.findContours(img_bin, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

        if cnt:
            for c in cnt:
                area = cv2.contourArea(c)
                if abs(area) < 300:
                    continue
                hull = cv2.convexHull(c)
                approx = cv2.approxPolyDP(hull, cv2.arcLength(c, True) * 0.02, True)

                if len(approx):
                    continue

                moments = cv2.moments(c)
                try:
                    x = int(moments["m10"] / moments["m00"])
                    y = int(moments["m01"] / moments["m00"])
                    return x, y
                finally:
                    return 0, 0

        return 0, 0

    def infinity_loop(self):
        while True:
            self._update_motors_power()

            # Самая первая стадия запуска, погружаемся
            self._try_centralize_depth()

            # Раз в секунду принтим отладочные данные
            if self.DEBUG_MODE and time.time() % 1000 == 0:
                print(f"Current depth: {self._mur_api.get_depth()}")
                print(f"Current yaw: {self._mur_api.get_yaw()}")
                print(f"Current pitch: {self._mur_api.get_pitch()}")
                print(f"Current roll: {self._mur_api.get_roll()}")

    def _update_motors_power(self):
        print(f"[INFO] Motor values: {self.motor_powers}")
        for index, value in enumerate(self.motor_powers):
            self._mur_api.set_motor_power(index, value)

    def keep_depth(self, depth_value: float, power_value: int = 50):
        print(f"[INFO] Keep Depth power value: {power_value}")

        depth_distance = self._mur_api.get_depth() - depth_value

        if abs(depth_distance) < 0.25:
            print(f"[INFO] Current depth distance: {depth_distance}")

            self.motor_powers[2] = 0
            self.motor_powers[3] = 0
            return

        power_2, power_3 = power_value, power_value

        if depth_distance > 0:
            power_2 = -power_value
            power_3 = -power_value

        self.motor_powers[2] = power_2
        self.motor_powers[3] = power_3

    def _rotate(self, rotation_speed=10):
        print(f"[INFO] Rotating... {rotation_speed}")
        self.motor_powers[0] = rotation_speed
        self.motor_powers[1] = -rotation_speed

    def _try_centralize_depth(self):
        """
        Функция вызывается каждый ход цикла, чтобы держать текущую глубину
        """
        current_depth = self._mur_api.get_depth()

        if self.is_dived_up:
            self.keep_depth(current_depth)
            return

        x, y = self.get_color_coordinates(self._get_front_image(), self.LOW_RED, self.HIGH_RED)
        current_state = self._get_current_depth_state(y)

        if current_state == self.STATES[0]:
            # Опускаемся

            self.keep_depth(current_depth - 1)

            self._rotate(0)
        elif current_state == self.STATES[1]:
            # Поднимаемся
            self.keep_depth(current_depth + 1)

            self._rotate(0)
        elif current_state == self.STATES[2]:
            # Записываем текущую глубину, остаёмся на ней
            self.is_dived_up = True

            self.dive_depth = self._mur_api.get_depth()
            self._rotate(0)
        else:
            # На фотке ничего не нашли, значит будем спускаться
            print("[DEBUG] Error, unhandled move, dive down")

            # TODO
            self.keep_depth(0, 10)
            self._rotate(-50)

    def _get_front_image(self):
        """
        camera = cv2.VideoCapture(0)
        for i in range(10):
            return_value, image = camera.read()
        """

        return self._mur_api.get_image_front()

    def _get_current_depth_state(self, y):
        if y <= 0:
            # print(y)
            return self.STATES[3]

        if self.max_range <= y <= self.min_range:
            return self.STATES[2]

        elif y < self.min_range:
            return self.STATES[1]

        return self.STATES[0]
